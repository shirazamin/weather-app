## Intro 
This is a simple weather app, which uses data from the Open Weather Map API.

Dependencies are handled by **npm**.

## Usage
**npm i** - to install dependencies

Needs to be used with this [WebAPI](https://bitbucket.org/shirazamin/weatherapp.api). 
Both need to be run to make them work locally.

## Sctipts
**npm start** for developing (it runs webpack-dev-server)  
**npm run build** to run a dev build  
**npm run clean** to clean build dir  
**npm run dev** to run a dev build with watching filesystem for changes  