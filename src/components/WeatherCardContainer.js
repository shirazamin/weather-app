import React, { Component } from "react";
import WeatherCard from "./../containers/WeatherCard";
import { Row, Button } from "reactstrap";

const WeatherCardContainer = ({ locations, isFetchingLocations }) => {
  let cards = [];
  Object.keys(locations).map(key => {
    if (locations[key].location) {
      let element = locations[key].location;
      cards.push(<WeatherCard key={key} locationName={element.LocationName} />);
    }
  });
  return (
    <div>
      <Row>{cards}</Row>
    </div>
  );
};

export default WeatherCardContainer;
