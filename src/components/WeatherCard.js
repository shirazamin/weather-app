import React, { Component } from "react";
import {
  Card,
  CardImg,
  CardText,
  CardBlock,
  CardTitle,
  CardSubtitle,
  CardHeader,
  CardFooter,
  Col,
  Button,
  Row
} from "reactstrap";

export default class WeatherCard extends Component {
  componentDidMount() {
    const { refreshWeather, location } = this.props;
    refreshWeather(location);
  }

  componentDidUpdate(prevProps) {
    if (this.props.didInvalidate) {
      const { refreshWeather, location } = this.props;
      refreshWeather(location);
    }
  }

  // Get the color for the card based on the weather codes
  // 2xx,6xx,3xx,5xx are blue to represent snow and rain
  // 800 is clear sky
  // 8xx are cloudy sky's and stronger weathers like hurricanes etc
  getCardColor(weather) {
    if (weather) {
      let id = weather.weather[0].id;
      if (id == 800) return "bg-yellow";
      let firstDigit = id.toString()[0];
      switch (firstDigit) {
        case "2":
        case "6":
        case "3":
        case "5":
          return "bg-blue";
        case "8":
        default:
          return "bg-gray";
      }
    } else {
      return "bg-gray";
    }
  }

  render() {
    const {
      location,
      didInvalidate,
      weather,
      isFetching,
      deleteLoc
    } = this.props;
    let display = !didInvalidate && !isFetching;
    let bgColor = this.getCardColor(weather);
    return (
      <Col sm="12" md="4" xs="12">
        <Card className={bgColor}>
          <CardBlock className="card-body ">
            <ul className="list-inline text-right font-lg">
              <li>
                <i
                  className="fa fa-times"
                  onClick={e => {
                    e.preventDefault();
                    deleteLoc(location);
                  }}
                />
              </li>
            </ul>
            <div className="weather-main">
              <div>
                {display ? (
                  <div>
                    <i
                      className={`wi wi-owm-day-${weather.weather[0]
                        .id} weather-icon`}
                    />
                    <span className="weather-desc">
                      {weather.weather[0].description}
                    </span>
                  </div>
                ) : (
                  <div>
                    <i className="fa fa-circle-o-notch fa-spin weather-icon" />
                    <span className="weather-desc">Loading</span>
                  </div>
                )}

                <span className="weather-location">
                  {location.LocationName}
                </span>
              </div>
            </div>
          </CardBlock>
          <CardFooter className="weather-details">
            {display ? (
              <Row>
                <Col xs="3" className="weather-info">
                  <i className="wi wi-thermometer" />
                  <span>{Math.round(weather.main.temp)}°</span>
                </Col>
                <Col xs="3" className="weather-info">
                  <i className="wi wi-humidity" />
                  <span>{weather.main.humidity}%</span>
                </Col>
                <Col xs="3" className="weather-info">
                  <i className="wi wi-strong-wind" />
                  <span>{Math.round(weather.wind.speed)} mph</span>
                </Col>
                <Col xs="3" className="weather-info">
                  <i className="wi wi-barometer" />
                  <span>{Math.round(weather.main.pressure)} hPa</span>
                </Col>
              </Row>
            ) : (
              <Row>
                <Col xs="12" className="weather-info">
                  <i className="wi wi-na" />
                  <span>Data not available</span>
                </Col>
              </Row>
            )}
          </CardFooter>
        </Card>
      </Col>
    );
  }
}
