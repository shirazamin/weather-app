import React, { Component } from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng
} from "react-places-autocomplete";
import PropTypes from "prop-types";
const AutocompleteItem = ({ suggestion }) => (
  <div>
    <i className="fa fa-map-marker" />
    {suggestion}
  </div>
);

class LocationSearchBar extends Component {
  constructor(props) {
    super(props);
    this.state = { address: "" };
    this.onChange = address => {
      this.setState({ address });
    };
  }

  render() {
    const { onSelect } = this.props;
    const handleSelect = (address, placeId) => {
      this.setState({ address: "", placeId });
      geocodeByAddress(address)
        .then(results => getLatLng(results[0]))
        .then(({ lat, lng }) => {
          onSelect({ LocationName: address, Latitude: lat, Longitude: lng });
        });
    };
    const inputProps = {
      value: this.state.address,
      onChange: this.onChange,
      type: "text",
      placeholder: "Search Places...",
      autoFocus: true
    };

    const options = {
      types: ["(cities)"]
    };

    const myStyles = {
      autocompleteContainer: { zIndex: 9999 }
    };

    return (
      <div className="mb-4">
        <PlacesAutocomplete
          inputProps={inputProps}
          autoCompleteItem={AutocompleteItem}
          onSelect={handleSelect}
          options={options}
          styles={myStyles}
        />
      </div>
    );
  }
}

LocationSearchBar.propTypes = {
  onSelect: PropTypes.func.isRequired
};

export default LocationSearchBar;
