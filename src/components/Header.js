import React, { Component } from "react";
import {
  Nav,
  NavItem,
  NavbarToggler,
  NavbarBrand,
  NavLink,
  Badge,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import PropTypes from "prop-types";
class Header extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    const { onLogoutClick, refreshAll, user } = this.props;
    return (
      <header className="app-header navbar">
        <NavbarToggler className="font-2xl mr-auto">
          <i
            className="wi wi-cloud-refresh"
            onClick={e => {
              e.preventDefault();
              refreshAll();
            }}
          />
        </NavbarToggler>
        <NavbarBrand href="#" />
        <Nav className="ml-auto" navbar>
          <NavItem>
            <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <DropdownToggle className="nav-link dropdown-toggle mr-2">
                <i className="fa fa-user-circle mr-1" />
                <span className="d-md-down-none">
                  {user.isFetching ? "" : user.user.FullName}
                </span>
              </DropdownToggle>
              <DropdownMenu
                right
                className={`mr-1 ${this.state.dropdownOpen ? "show" : ""}`}
              >
                <DropdownItem onClick={onLogoutClick}>
                  <i className="fa fa-lock" /> Logout
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </NavItem>
        </Nav>
      </header>
    );
  }
}

Header.propTypes = {
  onLogoutClick: PropTypes.func.isRequired
};

export default Header;
