import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  CardGroup,
  Card,
  CardBlock,
  Button,
  Input,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import { Redirect, Link } from "react-router-dom";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = { username: "", password: "" };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    const { onClick } = this.props;
    event.preventDefault();
    onClick(this.state);
  }
  render() {
    const { message, isAuthenticated, isFetching, redirectTo } = this.props;
    if (isAuthenticated) {
      return <Redirect to={redirectTo} />;
    }

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="4">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  <CardBlock className="card-body">
                    <h1>Login</h1>
                    <p className="text-muted">Sign In to your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon>
                        <i className="fa fa-user" />
                      </InputGroupAddon>
                      <Input
                        type="text"
                        name="username"
                        placeholder="Username"
                        value={this.state.username}
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon>
                        <i className="fa fa-lock" />
                      </InputGroupAddon>
                      <Input
                        type="password"
                        name="password"
                        placeholder="Password"
                        value={this.state.password}
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <Row>
                      <Col xs="12">
                        {message && <p className="text-danger">{message}</p>}
                      </Col>
                      <Col xs="3">
                        <Button
                          color="primary"
                          className="px-4"
                          onClick={this.handleSubmit}
                        >
                          {isFetching ? (
                            <i className="fa fa-spinner fa-spin" />
                          ) : (
                            "Login"
                          )}
                        </Button>
                      </Col>
                      <Col xs="9" className="text-right">
                        <Link to="/signup">Click here to sign up!</Link>
                      </Col>
                    </Row>
                  </CardBlock>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
