import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  CardGroup,
  Card,
  CardBlock,
  Button,
  Input,
  InputGroup,
  InputGroupAddon
} from "reactstrap";

import { Redirect } from "react-router-dom";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Email: "",
      Username: "",
      FirstName: "",
      LastName: "",
      Password: "",
      ConfirmPassword: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    const { onClick } = this.props;
    event.preventDefault();
    onClick(this.state);
  }
  render() {
    const { isSignedup, isFetching, message } = this.props;
    if (isSignedup) {
      return <Redirect to={{ pathname: "login" }} />;
    }

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="6">
              <CardGroup className="mb-0">
                <Card className="p-4">
                  <CardBlock className="card-body">
                    <h1>Sign Up</h1>
                    <p className="text-muted">Create your account</p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon>
                        <i className="fa fa-user" />
                      </InputGroupAddon>
                      <Input
                        type="text"
                        name="Username"
                        placeholder="Username"
                        value={this.state.Username}
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon>
                        <i className="fa fa-user" />
                      </InputGroupAddon>
                      <Input
                        type="text"
                        name="Email"
                        placeholder="Email"
                        value={this.state.Email}
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon>
                        <i className="fa fa-user" />
                      </InputGroupAddon>
                      <Input
                        type="text"
                        name="FirstName"
                        value={this.state.FirstName}
                        placeholder="First Name"
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-3">
                      <InputGroupAddon>
                        <i className="fa fa-user" />
                      </InputGroupAddon>
                      <Input
                        type="text"
                        name="LastName"
                        value={this.state.LastName}
                        placeholder="Last Name"
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon>
                        <i className="fa fa-lock" />
                      </InputGroupAddon>
                      <Input
                        type="password"
                        name="Password"
                        value={this.state.Password}
                        placeholder="Password"
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon>
                        <i className="fa fa-lock" />
                      </InputGroupAddon>
                      <Input
                        type="password"
                        name="ConfirmPassword"
                        value={this.state.ConfirmPassword}
                        placeholder="Confirm Password"
                        onChange={this.handleChange}
                      />
                    </InputGroup>
                    <Row>
                      <Col xs="3">
                        <Button
                          color="primary"
                          className="px-4"
                          onClick={this.handleSubmit}
                        >
                          {isFetching ? (
                            <i className="fa fa-spinner fa-spin" />
                          ) : (
                            "Sign Up"
                          )}
                        </Button>
                      </Col>
                      <Col xs="9" className="text-right">
                        {message && <p className="text-danger">{message}</p>}
                      </Col>
                    </Row>
                  </CardBlock>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Signup;
