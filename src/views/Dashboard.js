import React, { Component } from "react";
import LocationSearchBar from "./../containers/LocationSearchBar";
import WeatherCards from "./../containers/WeatherContainer";
import { Row } from "reactstrap";
const Dashboard = () => {
  return (
    <div>
      <LocationSearchBar />

      <WeatherCards />
    </div>
  );
};

export default Dashboard;
