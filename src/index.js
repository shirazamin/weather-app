import React from "react";
import ReactDOM from "react-dom";
import {
  HashRouter,
  Router,
  Route,
  Switch,
  browserHistory
} from "react-router-dom";
import { createBrowserHistory } from "history";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunkMiddleware from "redux-thunk";
import reducers from "./reducers/";
// Styles
// Import Font Awesome Icons Set
import "font-awesome/css/font-awesome.min.css";
// Import Main styles for this application
import "../scss/style.scss";

// Components
import Full from "./containers/Full";
import Login from "./containers/Login";
import Signup from "./containers/Signup";

// Setup redux
let createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);
let store = createStoreWithMiddleware(reducers);
const history = createBrowserHistory();

ReactDOM.render(
  <Provider store={store}>
    <HashRouter history={history}>
      <Switch>
        <Route exact path="/login" name="Login" component={Login} />
        <Route exact path="/signup" name="Signup" component={Signup} />
        <Route path="/" name="Home" component={Full} />
      </Switch>
    </HashRouter>
  </Provider>,
  document.getElementById("root")
);
