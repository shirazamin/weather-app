// config for login
export function loginConfig(creds) {
  return {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Access-Control-Allow-Origin": "*"
    },
    body: `username=${creds.username}&password=${creds.password}&grant_type=password`,
    mode: "cors"
  };
}

// Configurations for authenticated calls to the API
function getHeaders() {
  return {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    Authorization: `Bearer ${localStorage.getItem("access_token")}`
  };
}

export function PostConfig(body) {
  return {
    method: "POST",
    headers: getHeaders(),
    body: body,
    mode: "cors"
  };
}

export function PostUnAuthConfig(body) {
  return {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "Access-Control-Allow-Origin": "*"
    },
    body: body,
    mode: "cors"
  };
}

export function GetConfig() {
  return {
    method: "GET",
    headers: getHeaders(),
    mode: "cors"
  };
}

export function DeleteConfig() {
  return {
    method: "DELETE",
    headers: getHeaders(),
    mode: "cors"
  };
}
