import {
  REQUEST_NEW_LOCATION,
  POST_NEW_LOCATION,
  ERROR_NEW_LOCATION,
  RECEIVE_LOCATIONS,
  REQUEST_LOCATIONS,
  ERROR_LOCATIONS,
  DELETE_LOCATION,
  REQUEST_DELETE_LOCATION,
  DELETE_ALL_LOCATIONS
} from "./../actions/locations";

import {
  RECEIVE_WEATHER,
  REQUEST_WEATHER,
  ERROR_WEATHER
} from "./../actions/weather";

function locationWeather(
  state = {
    isFetching: false,
    didInvalidate: false
  },
  action
) {
  switch (action.type) {
    // Update location information
    case POST_NEW_LOCATION:
      return Object.assign({}, state, {
        didInvalidate: true,
        isFetching: false,
        location: action.location
      });
    case REQUEST_NEW_LOCATION:
    case REQUEST_DELETE_LOCATION:
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: true
      });
    case ERROR_NEW_LOCATION:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: true
      });
    // Update weather information for the location
    case RECEIVE_WEATHER:
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        weather: action.weather,
        location: action.location
      });
    case REQUEST_WEATHER:
      return Object.assign({}, state, {
        didInvalidate: true,
        isFetching: true,
        location: action.location
      });
    case ERROR_WEATHER:
      return Object.assign({}, state, {
        didInvalidate: true,
        isFetching: false,
        location: action.location,
        errorMessage: action.errorMessage
      });
    default:
      return state;
  }
}

// Updates locations and their weather information
export default function weatherByLocation(state = {}, action) {
  switch (action.type) {
    case REQUEST_NEW_LOCATION:
    case POST_NEW_LOCATION:
    case ERROR_NEW_LOCATION:
      return Object.assign({}, state, {
        [action.location.LocationName]: locationWeather(
          state[action.location.LocationName],
          action
        )
      });
    case RECEIVE_WEATHER:
    case REQUEST_WEATHER:
    case ERROR_WEATHER:
      return Object.assign({}, state, {
        [action.location.LocationName]: locationWeather(
          [action.location.LocationName],
          action
        )
      });
    case REQUEST_LOCATIONS:
      return Object.assign({}, state, {
        isFetching: true
      });
    case RECEIVE_LOCATIONS:
      return Object.assign({}, state, {
        isFetching: false
      });
    case ERROR_LOCATIONS:
      return Object.assign({}, state, {
        isFetching: false,
        errorMessage: action.errorMessage
      });
    case DELETE_LOCATION:
      let newState = Object.assign({}, state);
      delete newState[action.location.LocationName];
      return newState;
    case DELETE_ALL_LOCATIONS:
      return {};
    case REQUEST_DELETE_LOCATION:
      return Object.assign({}, state, {
        [action.location.LocationName]: locationWeather(
          [action.location.LocationName],
          action
        )
      });
    default:
      return state;
  }
}
