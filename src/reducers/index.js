import { combineReducers } from "redux";
import auth from "./auth";
import weatherByLocation from "./locations";
import user from "./user";
import signup from "./signup";
const rootReducers = combineReducers({
  auth: auth,
  weatherByLocation: weatherByLocation,
  user: user,
  signup: signup
});

export default rootReducers;
