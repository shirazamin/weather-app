import {
  SIGNUP_FAILURE,
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS
} from "./../actions/signup";

export default function signup(
  state = {
    isFetching: false,
    isSignedup: false
  },
  action
) {
  switch (action.type) {
    case SIGNUP_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        isSignedup: false,
        errorMessage: ""
      });
    case SIGNUP_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        isSignedup: true,
        errorMessage: ""
      });
    case SIGNUP_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        isSignedup: false,
        errorMessage: action.message
      });
    default:
      return state;
  }
}
