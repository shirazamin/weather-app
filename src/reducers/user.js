import {
  RECEIVE_USER,
  REQUEST_USER,
  ERROR_USER,
  REMOVE_USER
} from "./../actions/user";

// The user reducer.
export default function user(
  state = {
    isFetching: true,
    user: {}
  },
  action
) {
  switch (action.type) {
    case REQUEST_USER:
      return Object.assign({}, state, {
        isFetching: true
      });
    case REMOVE_USER:
      return Object.assign({}, state, {
        isFetching: false,
        user: {}
      });
    case RECEIVE_USER:
      return Object.assign({}, state, {
        isFetching: false,
        user: action.user
      });
    case ERROR_USER:
      return Object.assign({}, state, {
        isFetching: false,
        errorMessage: action.message
      });
    default:
      return state;
  }
}
