import { connect } from "react-redux";
import WeatherCard from "./../components/WeatherCard";
import { fetchWeather } from "./../actions/weather";
import { deleteLocationOnDatabase } from "./../actions/locations";

const mapDispatchToProps = dispatch => {
  return {
    refreshWeather: location => {
      dispatch(fetchWeather(location));
    },
    deleteLoc: location => {
      dispatch(deleteLocationOnDatabase(location));
    }
  };
};

const mapStateToProps = (state, ownProps) => {
  let locationName = ownProps.locationName;
  let locationInfo = state.weatherByLocation[locationName];

  return {
    location: locationInfo.location,
    didInvalidate: locationInfo.didInvalidate,
    weather: locationInfo.weather,
    isFetching: locationInfo.isFetching
  };
};

const WeatherCardContainer = connect(mapStateToProps, mapDispatchToProps)(
  WeatherCard
);

export default WeatherCardContainer;
