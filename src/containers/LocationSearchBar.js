import { connect } from "react-redux";
import LocationSearchBar from "./../components/LocationSearchBar";
import { insertNewLocation } from "./../actions/locations";

const mapDispatchToProps = dispatch => {
  return {
    onSelect: location => {
      dispatch(insertNewLocation(location));
    }
  };
};

const LocationSearchBarContainer = connect(null, mapDispatchToProps)(
  LocationSearchBar
);

export default LocationSearchBarContainer;
