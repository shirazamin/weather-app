import React, { Component } from "react";
import { Link, Switch, Route, Redirect } from "react-router-dom";
import { Container } from "reactstrap";
import Dashboard from "./../views/Dashboard";
import Header from "./Header";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchLocationsIfNeeded } from "./../actions/locations";
import { fetchUserIfNeeded } from "./../actions/user";

class Full extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.fetch();
  }

  componentDidUpdate(prevProps) {
    this.fetch();
  }

  fetch() {
    const { dispatch, isAuthenticated } = this.props;
    if (isAuthenticated) {
      dispatch(fetchUserIfNeeded());
      dispatch(fetchLocationsIfNeeded());
    }
  }

  render() {
    const { dispatch, isAuthenticated } = this.props;
    if (!isAuthenticated) {
      return (
        <Redirect
          to={{ pathname: "login", state: { from: this.props.location } }}
        />
      );
    }
    return (
      <div className="app">
        <Header />
        <div className="app-body">
          <main className="main">
            <Container fluid>
              <Switch>
                <Route path="/home" name="Home" component={Dashboard} />
                <Redirect from="/" to="/home" />
              </Switch>
            </Container>
          </main>
        </div>
      </div>
    );
  }
}

Full.propTypes = {
  dispatch: PropTypes.func.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  isFetchingLocations: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  const { auth, weatherByLocation } = state;
  return {
    isAuthenticated: auth.isAuthenticated,
    isFetchingLocations: weatherByLocation.isFetching || true
  };
};

export default connect(mapStateToProps)(Full);
