import { connect } from "react-redux";
import WeatherContainer from "./../components/WeatherCardContainer";

const mapStateToProps = state => {
  const { weatherByLocation } = state;
  return {
    locations: weatherByLocation,
    isFetchingLocation: weatherByLocation.isFetching
  };
};

const WeatherCardsContainer = connect(mapStateToProps, null)(WeatherContainer);

export default WeatherCardsContainer;
