import { connect } from "react-redux";
import Header from "./../components/Header";
import { logoutUser } from "./../actions/logout";
import { refreshAll } from "./../actions/weather";

const mapDispatchToProps = dispatch => {
  return {
    onLogoutClick: event => {
      dispatch(logoutUser());
    },
    refreshAll: event => {
      dispatch(refreshAll());
    }
  };
};

const mapStateToProps = state => {
  return {
    user: state.user
  };
};
const HeaderContainer = connect(mapStateToProps, mapDispatchToProps)(Header);

export default HeaderContainer;
