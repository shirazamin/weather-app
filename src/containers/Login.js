import { connect } from "react-redux";
import Login from "./../views/Login";
import { loginUser } from "./../actions/login";

const mapDispatchToProps = dispatch => {
  return {
    onClick: event => {
      dispatch(loginUser(event));
    }
  };
};

const mapStateToProps = (state, ownProps) => {
  const { from } = ownProps.location.state || { from: { pathname: "/" } };
  return {
    message: state.auth.errorMessage ? state.auth.errorMessage : "",
    isAuthenticated: state.auth.isAuthenticated,
    redirectTo: from,
    isFetching: state.auth.isFetching
  };
};

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login);

export default LoginContainer;
