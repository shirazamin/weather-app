import { connect } from "react-redux";
import Signup from "./../views/Signup";
import { signupUser } from "./../actions/signup";

const mapDispatchToProps = dispatch => {
  return {
    onClick: event => {
      dispatch(signupUser(event));
    }
  };
};

const mapStateToProps = state => {
  const { signup } = state;
  return {
    message: signup.errorMessage ? signup.errorMessage : "",
    isSignedup: signup.isSignedup,
    isFetching: signup.isFetching
  };
};

const SignupContainer = connect(mapStateToProps, mapDispatchToProps)(Signup);

export default SignupContainer;
