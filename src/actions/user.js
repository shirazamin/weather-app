import fetch from "isomorphic-fetch";
import { GetConfig } from "./../utility/apiConfig";

export const REQUEST_USER = "REQUEST_USER";
export const RECEIVE_USER = "RECEIVE_USER";
export const ERROR_USER = "ERROR_USER";
export const REMOVE_USER = "LOGOUT_USER";

function requestUser() {
  return {
    type: REQUEST_USER,
    isFetching: true
  };
}

function receiveUser(user) {
  return {
    type: RECEIVE_USER,
    user: user
  };
}

export function removeUser() {
  return {
    type: REMOVE_USER
  };
}

function errorUser(message) {
  return {
    type: ERROR_USER,
    errorMessage: message
  };
}

function fetchUser() {
  let config = GetConfig();

  return dispatch => {
    // We dispatch to kickoff the call to the API
    dispatch(requestUser());
    var url = API_URL;
    return fetch(`${url}/api/user`, config)
      .then(response => response.json().then(user => ({ user, response })))
      .then(({ user, response }) => {
        if (!response.ok) {
          // If there was a problem, we want to
          // dispatch the error condition
          dispatch(errorUser(user.message));
          return Promise.reject(user);
        } else {
          // Dispatch the success action
          dispatch(receiveUser(user));
        }
      })
      .catch(err => {
        let errorMessage = "Could not get user information!";
        dispatch(errorUser(errorMessage));
      });
  };
}

export function fetchUserIfNeeded() {
  return (dispatch, getState) => {
    let state = getState();
    if (state.user.user) {
      return dispatch(fetchUser());
    }
  };
}
