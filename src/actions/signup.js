import fetch from "isomorphic-fetch";
import { PostUnAuthConfig } from "./../utility/apiConfig";

// There are three possible states for our signup
// process and we need actions for each of them
export const SIGNUP_REQUEST = "SIGNUP_REQUEST";
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const SIGNUP_FAILURE = "SIGNUP_FAILURE";

function requestSignup(creds) {
  return {
    type: SIGNUP_REQUEST,
    isFetching: true,
    isSignedUp: false
  };
}

function receiveSignup() {
  return {
    type: SIGNUP_SUCCESS,
    isFetching: false,
    isSignedUp: true
  };
}

export function signupError(message) {
  return {
    type: SIGNUP_FAILURE,
    isFetching: false,
    isSignup: false,
    message
  };
}

// Calls the API to get a token and
// dispatches actions along the way
export function signupUser(creds) {
  let config = PostUnAuthConfig(JSON.stringify(creds));

  return dispatch => {
    // We dispatch  to kickoff the call to the API
    dispatch(requestSignup(creds));
    var url = API_URL;
    return fetch(`${url}/api/user/create`, config)
      .then(response => response.json().then(user => ({ user, response })))
      .then(({ user, response }) => {
        if (!response.ok) {
          // If there was a problem, we want to
          // dispatch the error condition
          return Promise.reject(user);
        } else {
          // Dispatch the success action
          dispatch(receiveSignup());
        }
      })
      .catch(err => {
        let message = err.ModelState;
        let errorMessage = "Could not sign up!";
        if (message) {
          errorMessage = message[Object.keys(message)[0]][0];
        }
        dispatch(signupError(errorMessage));
      });
  };
}
