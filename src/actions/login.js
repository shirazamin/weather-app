import fetch from "isomorphic-fetch";
import { loginConfig } from "./../utility/apiConfig";

// There are three possible states for our login
// process and we need actions for each of them
export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";

function requestLogin(creds) {
  return {
    type: LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    creds
  };
}

function receiveLogin(user) {
  return {
    type: LOGIN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    access_token: user.access_token
  };
}

export function loginError(message) {
  localStorage.removeItem("access_token");
  return {
    type: LOGIN_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    message
  };
}

// Calls the API to get a token and
// dispatches actions along the way
export function loginUser(creds) {
  let config = loginConfig(creds);

  return dispatch => {
    // We dispatch requestLogin to kickoff the call to the API
    dispatch(requestLogin(creds));
    var url = API_URL;
    return fetch(`${url}/oauth/token`, config)
      .then(response => response.json().then(user => ({ user, response })))
      .then(({ user, response }) => {
        if (!response.ok) {
          // If there was a problem, we want to
          // dispatch the error condition
          dispatch(loginError(user.message));
          return Promise.reject(user);
        } else {
          // If login was successful, set the token in local storage
          localStorage.setItem("access_token", user.access_token);
          // Dispatch the success action
          dispatch(receiveLogin(user));
        }
      })
      .catch(err => {
        let errorMessage = "Could not login!";
        if (err.error == "invalid_grant") {
          errorMessage = err.error_description;
        }
        dispatch(loginError(errorMessage));
      });
  };
}
