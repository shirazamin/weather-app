import fetch from "isomorphic-fetch";

// States in getting weather information
export const REQUEST_WEATHER = "REQUEST_WEATHER";
export const RECEIVE_WEATHER = "RECEIVE_WEATHER";
export const ERROR_WEATHER = "ERROR_WEATHER";

function requestWeather(location) {
  return {
    type: REQUEST_WEATHER,
    location: location,
    isFetching: true
  };
}

function receiveWeather(location, weather) {
  return {
    type: RECEIVE_WEATHER,
    location: location,
    weather: weather
  };
}

function errorWeather(location, message) {
  return {
    type: ERROR_WEATHER,
    location: location,
    errorMessage: message
  };
}

export function refreshAll() {
  return (dispatch, getState) => {
    let state = getState();
    let locations = state.weatherByLocation;
    Object.keys(locations).map(key => {
      if (locations[key].location) {
        let element = locations[key].location;
        dispatch(requestWeather(element));
      }
    });
  };
}

export function fetchWeather(location) {
  let config = {
    method: "GET"
  };
  return dispatch => {
    // dispatch request being send to the weather api
    dispatch(requestWeather(location));
    var url = OWM_API_URL;
    return fetch(
      `${url}/weather?lat=${location.Latitude}&lon=${location.Longitude}&APPID=${OWM_API_KEY}&units=imperial`,
      config
    )
      .then(response => {
        if (!response.ok) {
          dispatch(
            errorWeather(location, "Could not get weather for this location")
          );
          return Promise.reject(location);
        } else {
          response.json().then(weather => {
            // dispatch the new weather information for the location
            dispatch(receiveWeather(location, weather));
          });
        }
      })
      .catch(err => {
        let errorMessage = "Error loading weather information";
        dispatch(errorWeather(errorMessage));
      });
  };
}
