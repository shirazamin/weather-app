import fetch from "isomorphic-fetch";
import { PostConfig, GetConfig, DeleteConfig } from "./../utility/apiConfig";
import { loginError } from "./login";

// These are the actions that will happen
export const REQUEST_NEW_LOCATION = "REQUEST_NEW_LOCATION";
export const POST_NEW_LOCATION = "POST_NEW_LOCATION";
export const ERROR_NEW_LOCATION = "ERROR_NEW_LOCATION";
export const REQUEST_LOCATIONS = "REQUEST_LOCATIONS";
export const RECEIVE_LOCATIONS = "RECEIVE_LOCATIONS";
export const ERROR_LOCATIONS = "ERROR_LOCATIONS";
export const REQUEST_DELETE_LOCATION = "REQUEST_DELETE_LOCATION";
export const DELETE_LOCATION = "DELETE_LOCATION";
export const DELETE_ALL_LOCATIONS = "DELETE_ALL_LOCATIONS";

function requestLocation(location) {
  return {
    type: REQUEST_NEW_LOCATION,
    isFetching: true,
    location
  };
}

function postLocation(location) {
  return {
    type: POST_NEW_LOCATION,
    isFetching: false,
    location
  };
}

function errorLocation(location, message) {
  return {
    type: ERROR_NEW_LOCATION,
    isFetching: false,
    location,
    errorMessage: message
  };
}

function requestLocations() {
  return {
    type: REQUEST_LOCATIONS,
    isFetching: true
  };
}

function receiveLocations() {
  return {
    type: RECEIVE_LOCATIONS,
    isFetching: false
  };
}

function errorFetchingLocations(message) {
  return {
    type: ERROR_LOCATIONS,
    isFetching: false,
    errorMessage: message
  };
}

function requestDeleteLocation(location) {
  return {
    type: REQUEST_DELETE_LOCATION,
    location: location
  };
}

function deleteLocation(location) {
  return {
    type: DELETE_LOCATION,
    location: location
  };
}

export function deleteAllLocations() {
  return {
    type: DELETE_ALL_LOCATIONS
  };
}

export function deleteLocationOnDatabase(location) {
  // Get the authenticated delete config
  let config = DeleteConfig();
  return dispatch => {
    // first dispatch new location is requested
    dispatch(requestDeleteLocation(location));
    var url = API_URL;
    return fetch(`${url}/api/locations/${location.Id}`, config)
      .then(response => {
        if (!response.ok) {
          if (response.status == 401) {
            // Unauthorized, dispatch logout
            dispatch(loginError(response.statusText));
          }
          return Promise.reject(location);
        } else {
          // Dispatch when location successfully added to the database
          dispatch(deleteLocation(location));
        }
      })
      .catch(err => {
        let errorMessage = "Error deleting location";
      });
  };
}

export function insertNewLocation(location) {
  // Get the authenticated post config
  let config = PostConfig(JSON.stringify(location));
  return dispatch => {
    // first dispatch new location is requested
    dispatch(requestLocation(location));
    var url = API_URL;
    return fetch(`${url}/api/locations`, config)
      .then(response => {
        if (!response.ok) {
          if (response.status == 401) {
            // Unauthorized, dispatch logout
            dispatch(loginError(response.statusText));
          }
          dispatch(
            errorLocation(location, "Error adding location to database")
          );
          return Promise.reject(location);
        } else {
          // Dispatch when location successfully added to the database
          dispatch(postLocation(location));
        }
      })
      .catch(err => {
        let errorMessage = "Error adding location to database";
        dispatch(errorLocation(location, errorMessage));
      });
  };
}

function fetchLocations() {
  let config = GetConfig();
  return dispatch => {
    // first dispatch locations are requested
    dispatch(requestLocations());
    var url = API_URL;
    return fetch(`${url}/api/locations`, config)
      .then(response => {
        if (!response.ok) {
          if (response.status == 401) {
            // Unauthorized, dispatch logout
            dispatch(loginError(response.statusText));
          }
          return Promise.reject();
        } else {
          // Dispatch when locations received
          dispatch(receiveLocations());
          response.json().then(locations => {
            // Add each location to redux data store
            locations.forEach(function(element) {
              dispatch(postLocation(element));
            }, this);
          });
        }
      })
      .catch(err => {
        let errorMessage = "Error loading locations from database";
        dispatch(errorFetchingLocations(errorMessage));
      });
  };
}

function shouldFetchLocations(state) {
  const locations = state.weatherByLocations || [];
  if (locations.length == 0) {
    return true;
  }
  return false;
}

export function fetchLocationsIfNeeded() {
  return (dispatch, getState) => {
    if (shouldFetchLocations(getState())) {
      return dispatch(fetchLocations());
    }
  };
}
